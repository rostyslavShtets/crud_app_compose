package models

import (
	"database/sql"
	_ "github.com/go-sql-driver/mysql"
	"log"
	"time"
	"fmt"
)

var db *sql.DB

func InitDB(dataSourceName string) {
	var err error
	db, err = sql.Open("mysql", dataSourceName)
	if err != nil {
		log.Fatal(err)
	}
	// defer db.Close()

	err = db.Ping()
	if err != nil {
		for i := 1; i < 4; i++ {
			fmt.Printf("We are trying to connect to db, attempt %v...\n", i)
			time.Sleep(1500 * time.Millisecond)
			db, _ = sql.Open("mysql", dataSourceName)
			err = db.Ping()
			if err == nil {
				fmt.Printf("Database is connected!")
				return
			}
		}
		log.Fatal(err)
	}
}


	// stmt, err := db.Prepare("CREATE TABLE people (userID int NOT NULL AUTO_INCREMENT PRIMARY KEY, FirstName varchar(40), LastName varchar(40), Age int(3));")
	// if err != nil {
	// 	fmt.Println(err.Error())
	// }
	// _, err = stmt.Exec()
	// if err != nil {
	// 	fmt.Println(err.Error())
	// } else {
	// 	fmt.Println("Person Table successfully migrated....")
	// }
